#!/bin/bash
# Create releases for distribution.
# File inception by Huki (2015-05-01).

# Get version
DATE=$(date +"%y.%m%d")
SUFFIX="a"

BUILD=$(cd ../devel && ./build.sh && ./bin/version)
if [ -z "${BUILD}" ]; then
  BUILD="${DATE}${SUFFIX}"
fi

# Create folders
./init.sh

# Copy binaries
cp -r ./bin/* ./platform

# Build Windows setup
(cd ../devel/windows/setup && ./build.sh --win32)
(cd ../devel/windows/setup && ./build.sh --win64)

# Linux release
filename="rvgl_${BUILD}_linux.7z"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

(cd ./assets && 7z a ../$filename ./* -mx=9 -ms)
(cd ./platform/linux && 7z a ../../$filename ./* -mx=9 -ms)

# Win32 release
filename="rvgl_${BUILD}_win32.7z"
setup="rvgl_setup_win32.exe"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

(cd ./assets && 7z a ../$filename ./* -mx=9 -ms)
(cd ./platform/win32 && 7z a ../../$filename ./* -mx=9 -ms)

if [ -f ./$setup ]; then
  package="$filename"
  filename="rvgl_${BUILD}_setup_win32.exe"

  if [ -f ./$filename ]; then
    rm -f ./$filename
  fi

  cat ./$setup ./$package > ./$filename
  rm -f ./$setup
fi

# Win64 release
filename="rvgl_${BUILD}_win64.7z"
setup="rvgl_setup_win64.exe"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

(cd ./assets && 7z a ../$filename ./* -mx=9 -ms)
(cd ./platform/win64 && 7z a ../../$filename ./* -mx=9 -ms)

if [ -f ./$setup ]; then
  package="$filename"
  filename="rvgl_${BUILD}_setup_win64.exe"

  if [ -f ./$filename ]; then
    rm -f ./$filename
  fi

  cat ./$setup ./$package > ./$filename
  rm -f ./$setup
fi

# Dreamcast pack
filename="rvgl_dcpack.7z"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

(cd ./dcpack && 7z a ../$filename ./* -mx=9 -ms)

# Android package
filename="rvgl_${BUILD}_android.apk"
package="rvgl_android.apk"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

if [ -f ./$package ]; then
  cp ./$package $filename
  rm -f ./$package
fi
