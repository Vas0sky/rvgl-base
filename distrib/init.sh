#!/bin/bash
# Initial bootstrapping.
# File inception by Huki (2015-05-01).

for i in "$@"; do
  case $i in
    --clean) clean=true;;
  esac
done

# Clean directory?
if [ "$clean" = true ]; then
  rm -rf ./bin
  rm -f *.exe
  rm -f *.apk
  rm -f *.7z
fi

# Init assets
if [ -e "assets/.git" ]; then
  mkdir -p assets/cache
  mkdir -p assets/profiles
  mkdir -p assets/replays
  mkdir -p assets/times
fi

# Init bin
mkdir -p bin/linux
mkdir -p bin/win32
mkdir -p bin/win64

android=(
  "armeabi-v7a"
  "arm64-v8a"
  "x86"
  "x86_64"
)

for name in ${android[@]}; do
  mkdir -p bin/android/$name
done
