# RVGL Base Repository

This is the base public repository for RVGL development. The following 
repositories are included as sub-modules:

- [rvgl-assets](https://gitlab.com/re-volt/rvgl-assets) (`distrib/assets`): Common assets.
- [rvgl-dcpack](https://gitlab.com/re-volt/rvgl-dcpack) (`distrib/dcpack`): Dreamcast pack.
- [rvgl-platform](https://gitlab.com/re-volt/rvgl-platform) (`distrib/platform`): Platform binaries.
- [rvgl-devel](https://gitlab.com/re-volt/rvgl-devel) (`devel`): Development tools.
- [rvgl-android](https://gitlab.com/re-volt/rvgl-android) (`android`): Android project.
- [rvgl-core](https://gitlab.com/gk7huki/rvgl-core) (`core`): Private core repo.

## Setting Up

Use `git clone --recursive` to get needed sub-modules.

To get started, run: `./bootstrap.sh`. This will create an empty `data` folder
where you must place RVGL assets. If you want to use an existing RVGL installation,
run `./bootstrap.sh --data-dir="/path/to/RVGL"`.

